(function ($, Drupal) {

    Drupal.behaviors.BannerSlider = {
        attach: function (context, settings) {

            /* Slider secondary*/
            $('.view-speaker .view-content').once('BannerSlider').slick({
                slidesToShow: 4,
                slidesToScroll: 1,
                autoplay: true,
                autoplaySpeed: 3000,
                speed: 800,
                fade: false,
                pauseOnHover: true,
                focusOnSelect: false,
                swipeToSlide: true,
                infinite: true,
                arrows: true,
                centerMode: false,
                adaptiveHeight: true,
                //dots: true
                responsive: [
                    {
                        breakpoint: 1024,
                        settings: {
                            slidesToShow: 3
                        }
                    },
                    {
                        breakpoint: 768,
                        settings: {
                            slidesToShow: 2
                        }
                    },
                    {
                        breakpoint: 500,
                        settings: {
                            slidesToShow: 1
                        }
                    }
                ]
            });


            /*-----------------  Go To Top  --------------------*/
            var top = $('.top');
            $(window).scroll(function () {
                //$('html, body').stop( true, true );
                if ($(this).scrollTop() > 100) {
                    top.addClass('active');
                } else {
                    $('html, body').stop( true, true );
                    top.removeClass('active', 'fade');
                }
            });

            top.click(function () {
                top.addClass('fade');
                $('html, body').animate({scrollTop: 0}, 800);
                return false;
            });


            /*-----------------  Map Overlay  --------------------*/
            var map = $(".field--name-field-map");

            map.on('click', function(event){
                map.addClass('overlay-hide');
                event.stopPropagation();
            });

            $('html').click(function(){
                map.removeClass('overlay-hide');
            });

        }
    };

})(jQuery, Drupal);
